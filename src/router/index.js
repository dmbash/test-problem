import { createRouter, createWebHistory } from 'vue-router';
import MoviesView from '../views/MoviesView.vue';
import TVView from '../views/TVView.vue';

const routes = [
  {
    path: '/',
    alias: '/movies/',
    name: 'Movies',
    component: MoviesView,
  },
  {
    path: '/tv/',
    name: 'TV',
    component: TVView,
  },
];

export default createRouter({
  history: createWebHistory(),
  routes: [...routes],
});
