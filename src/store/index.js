import { createStore } from 'vuex';

export const getters = {
  getUsername: (state) => state.username,
};

export const mutations = {
  SET_USERNAME(state, username) {
    // eslint-disable-next-line
    state.username = username;
  },
  LOGOUT(state) {
    // eslint-disable-next-line
    state.username = "";
  },
};

export const state = () => ({
  username: localStorage.getItem('username') || '',
});

export const actions = {
  setUsername({ commit }, username) {
    if (typeof username === 'string' && username.length > 0) {
      commit('SET_USERNAME', username);
    }
  },
  logout({ commit }) {
    commit('LOGOUT');
  },
};

export default createStore({
  state,
  getters,
  mutations,
  actions,
});
