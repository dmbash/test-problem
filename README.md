# test-problem

[![Netlify Status](https://api.netlify.com/api/v1/badges/4ee84920-45b1-494c-b902-3eeb3a48c75e/deploy-status)](https://app.netlify.com/sites/xenodochial-elion-240185/deploys)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
